# NextGrowth Labs

# Front end engineer assignment

Here are short descriptions for both Section A and Section B projects:

**Section A: Building a Simple Website with Vanilla JS**

In Section A, the project involves creating a simple website using Bootstrap and vanilla JavaScript. The primary tasks include implementing a modal that appears when the user clicks on pricing options, allowing users to enter their name, email, and order comments. Additionally, a slider is added to highlight pricing plans based on the number of users selected. The project demonstrates how to use Bootstrap components and customize them with JavaScript to enhance the user experience.

**Section B: Lazy Loading and Pagination Avoidance with Vanilla JS**

Section B focuses on enhancing user experience by implementing lazy loading to avoid traditional pagination in web applications. The project generates random data and uses vanilla JavaScript to load more results automatically as the user scrolls down the page. It also includes sorting and filtering options for the loaded data. The goal is to create a responsive and efficient web application that seamlessly loads additional content without the need for manual pagination, providing users with a smoother browsing experience.

# PROJECT LINK - https://frontenddeveloperassignment.netlify.app/

# PORTFOLIO LINK - https://simratsingh.netlify.app/
