
const totalResults = 300; // Total number of results
let visibleResults = 15; // Number of results initially visible
const resultsContainer = document.getElementById('results');
const loadingIndicator = document.getElementById('loading');
const sortSelect = document.getElementById('sort');
const filterInput = document.getElementById('filter');

// Function to generate a random result
function generateRandomResult() {
    const imageUrl = `https://picsum.photos/200/200?random=${Math.floor(Math.random() * 1000)}`;
    return { imageUrl, text: Math.random().toString(36).substring(7) };
}

// Function to load more results
function loadMoreResults() {
    loadingIndicator.style.display = 'block';
    setTimeout(() => {
        for (let i = 0; i < 20; i++) {
            const resultData = generateRandomResult();
            const resultElement = document.createElement('div');
            resultElement.className = 'result-item';
            const resultImage = document.createElement('img');
            resultImage.src = resultData.imageUrl;
            const resultText = document.createElement('div');
            resultText.className = 'random-text'; // Add class for styling
            resultText.textContent = resultData.text;
            resultElement.appendChild(resultImage);
            resultElement.appendChild(resultText);
            resultsContainer.appendChild(resultElement);
        }
        visibleResults += 15;
        loadingIndicator.style.display = 'none';

        // Check if all results have been loaded
        if (visibleResults >= totalResults) {
            window.removeEventListener('scroll', onScroll);
        }
    }, 1200); 
}

// Function to handle scroll event
function onScroll() {
    if (window.innerHeight + window.scrollY >= document.body.offsetHeight) {
        loadMoreResults();
    }
}

// Function to sort results
function sortResults() {
    const resultItems = Array.from(resultsContainer.querySelectorAll('.result-item'));
    const sortOrder = sortSelect.value;
    resultItems.sort((a, b) => {
        const textA = a.textContent.toLowerCase();
        const textB = b.textContent.toLowerCase();
        if (sortOrder === 'asc') {
            return textA.localeCompare(textB);
        } else {
            return textB.localeCompare(textA);
        }
    });
    resultsContainer.innerHTML = '';
    resultItems.forEach(item => {
        resultsContainer.appendChild(item);
    });
}

// Function to filter results
function filterResults() {
    const keyword = filterInput.value.toLowerCase();
    const resultItems = Array.from(resultsContainer.querySelectorAll('.result-item'));
    resultItems.forEach(item => {
        const text = item.textContent.toLowerCase();
        if (text.includes(keyword)) {
            item.style.display = 'block';
        } else {
            item.style.display = 'none';
        }
    });
}

// Event listeners
sortSelect.addEventListener('change', sortResults);
filterInput.addEventListener('input', filterResults);

// Initial load
loadMoreResults();

// Add a scroll event listener
window.addEventListener('scroll', onScroll);