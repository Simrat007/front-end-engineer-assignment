
  const slider = document.getElementById('userSlider');
  const highlightedPlan = document.getElementById('highlightedPlan');
  const sliderValue = document.getElementById('sliderValue');
  const plans = document.querySelectorAll('.card-body');
  const plansheader = document.querySelectorAll('.card-header');

  // Function to highlight the plan based on the number of users
  function highlightPlan() {
    const numberOfUsers = parseInt(slider.value);
    
    // Remove any existing highlighting
    plans.forEach(plan => {
      plan.classList.remove('highlighted');
    });

    plansheader.forEach(plansheader => {
        plansheader.classList.remove('highlighted');
      });

    // Highlight the appropriate plan
    if (numberOfUsers >= 0 && numberOfUsers < 11) {
      plans[0].classList.add('highlighted');
      plansheader[0].classList.add('highlighted');
      highlightedPlan.textContent = '[ You Should Choose Free Plan ]';
    } else if (numberOfUsers >= 11 && numberOfUsers < 21) {
      plans[1].classList.add('highlighted');
      plansheader[1].classList.add('highlighted');
      highlightedPlan.textContent = '[ You Should Choose Pro Plan ]';
    } else if (numberOfUsers >= 21 && numberOfUsers <= 100) {
      plans[2].classList.add('highlighted');
      plansheader[2].classList.add('highlighted');
      highlightedPlan.textContent = '[ You Should Choose Enterprise Plan ]';
    }
    
    // Update the slider value text
    sliderValue.textContent = numberOfUsers + ' USERs';
  }

  // Add an input event listener to the slider
  slider.addEventListener('input', highlightPlan);

  // Initialize the highlighting
  highlightPlan();
  
  // Add smooth scrolling to the highlighted plan
  highlightedPlan.addEventListener('click', function() {
    plans.forEach(plan => {
      plan.classList.remove('highlighted');
    });
    
    // Determine which plan is highlighted and scroll to it
    const highlightedIndex = Array.from(plans).findIndex(plan => plan.classList.contains('highlighted'));
    if (highlightedIndex !== -1) {
      plans[highlightedIndex].scrollIntoView({ behavior: 'smooth' });
    }
  });
