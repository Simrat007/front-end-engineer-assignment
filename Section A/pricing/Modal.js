
  // Function to show the modal
  function showModal() {
    $('#orderModal').modal('show');
  }

  // Add click event listeners to the pricing buttons
  document.getElementById('freeBtn').addEventListener('click', showModal);
  document.getElementById('proBtn').addEventListener('click', showModal);
  document.getElementById('enterpriseBtn').addEventListener('click', showModal);

